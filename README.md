[![pipeline status](https://gitlab.com/nicosingh/rpi-rtl-sdr/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-rtl-sdr/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-rtl-sdr.svg)](https://hub.docker.com/r/nicosingh/rpi-rtl-sdr/)

# About

Docker image of Raspbian with DVB-T USB drivers. It is part of the following stack:

**rpi-rtl-sdr** -> rpi-dump1090 -> rpi-fr24feed

Which has the purpose of configuring a homemade FlightRadar24 server using a Raspberry Pi and a DVB-T stick.

# How to use this Docker image?

As this image just configures a USB stick driver in Raspbian, probably the only way to use it is including this image in another Docker image :)

`FROM nicosingh/rpi-rtl-sdr`
