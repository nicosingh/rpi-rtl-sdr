FROM resin/rpi-raspbian:stretch-20180214

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# Download required software
RUN apt-get update && \
  apt-get -y install cmake gcc pkg-config libusb-1.0 make git-core libc-dev wget && \
  rm -rf /var/lib/apt/lists/*

# Download and install DVB-T driver
WORKDIR /tmp
RUN git clone git://git.osmocom.org/rtl-sdr.git &&\
  mkdir -p rtl-sdr/build &&\
  cd rtl-sdr/build &&\
  cmake ../ -DINSTALL_UDEV_RULES=ON -DDETACH_KERNEL_DRIVER=ON &&\
  make &&\
  make install

RUN ldconfig

# Blacklist DVB-T module
RUN bash -c "echo 'blacklist dvb_usb_rtl28xxu' >> /etc/modprobe.d/no-rtl.conf"
RUN update-initramfs -u
